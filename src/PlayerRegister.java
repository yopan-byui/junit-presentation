import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class PlayerRegister {

    Set<Player> playerList = new HashSet<>();

    public void addPlayer(String name, Integer level) {
        Player player = new Player(name, level);

        validatePlayerName(player);
        validatePlayerlevel(player);
        playerList.add(player);
    }

    public Collection<Player> getAllPlayers() {
        return playerList;
    }

    private void validatePlayerName(Player player) {
        player.validatePlayerName();
    }

    private void validatePlayerlevel(Player player) {
        player.validatePlayerLevel();
    }
}
