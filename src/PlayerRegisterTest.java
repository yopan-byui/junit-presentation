import org.junit.jupiter.api.*;

@DisplayName("Registering a New Player")
public class PlayerRegisterTest {
    PlayerRegister playerRegister;
    String playerName;
    Integer playerLevel;

    @BeforeEach
    public void setupForEach() {
        playerRegister = new PlayerRegister();
        playerName = "Derek";
        playerLevel = 4;
    }

    @Test
    @DisplayName("Should successfully register a new player")
    public void shouldRegisterPlayerSuccessfully() {

        playerRegister.addPlayer(playerName, playerLevel);

        Assertions.assertFalse(playerRegister.getAllPlayers().isEmpty());
        Assertions.assertTrue(playerRegister.getAllPlayers().stream()
                .anyMatch(player -> player.getName().equals(playerName) &&
                        player.getLevel().equals(playerLevel)));
    }

    @Nested
    @DisplayName("When proper params are not provided")
    class NegativeRoute {
        @Test
        @DisplayName("Should raise an exception when player name is null")
        public void shouldThrowAnExceptionWhenNameIsNull() {
            playerName = null;

            Assertions.assertThrows(RuntimeException.class, () -> playerRegister.addPlayer(playerName, playerLevel));
        }

        @Test
        @DisplayName("Should raise an exception when player level is negative")
        public void shouldThrowAnExceptionWhenPlayerLevelIsNegative() {
            playerLevel = -2;

            Assertions.assertThrows(RuntimeException.class, () -> playerRegister.addPlayer(playerName, playerLevel));
        }
    }
}
